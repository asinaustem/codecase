//
//  SearchResultTableViewCell.swift
//  CodeCaseMarti
//
//  Created by AppLogist on 20.01.2020.
//  Copyright © 2020 AhmetSina. All rights reserved.
//

import UIKit

class SearchResultTableViewCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        accessoryType = .disclosureIndicator
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        accessoryType = .disclosureIndicator
    }
    
    func updateUI(viewModel: SearchResultTableCellViewModel) {
        textLabel?.text = viewModel.text
        detailTextLabel?.text = viewModel.detailText
    }
}
