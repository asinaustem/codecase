//
//  SearchViewController.swift
//  CodeCaseMarti
//
//  Created by AppLogist on 19.01.2020.
//  Copyright © 2020 AhmetSina. All rights reserved.
//

import UIKit
import SnapKit
import Moya
import ReactiveSwift

class SearchViewController: UIViewController {

    lazy var searchBar = UISearchBar(frame: .zero)
    lazy var tableView = UITableView()
    lazy var searchController = UISearchController(searchResultsController: nil)
    lazy var messageLabel = UILabel()
    
    // MARK: - Injection
    lazy var viewModel = SearchViewModel(service: NetworkManager())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    
    // MARK: - Networking
    private func searchPlaces(with query: String) {
        viewModel.searchPlaces(with: query)
        viewModel.showAlertClosure = {  [weak self] message in
            self?.updateMessageLabel(isHidden: false,
                                     messageText: message)
        }
        viewModel.didFinishFetch = { [weak self] in
            self?.tableView.reloadData()
            self?.updateMessageLabel(isHidden: true)
        }
    }
    
    
    // MARK: - Setup UI
    
    func setupUI() {
        view.backgroundColor = .white
        setupSearchbar()
        setupTableView()
        setupErrorLabel()
    }
    
    /// Setup search bar in navigationBar
    func setupSearchbar() {
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = "Places"
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search places"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    /// Setup Table View
    func setupTableView() {
        view.addSubview(tableView)
        tableView.backgroundColor = .clear
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = 60
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    
    /// Setup Error Label
    func setupErrorLabel() {
        view.addSubview(messageLabel)
        messageLabel.numberOfLines = 0
        messageLabel.text = "Search Now"
        messageLabel.backgroundColor = view.backgroundColor
        messageLabel.textAlignment = .center
        messageLabel.snp.makeConstraints { (make) in
            make.edges.equalToSuperview().inset(20)
        }
    }
    
    
    /// Update Message Label depends on data
    /// - Parameters:
    ///   - isHidden: Message Label visible value
    ///   - messageText: Message text presented in label
    func updateMessageLabel(isHidden: Bool, messageText: String? = nil) {
        messageLabel.isHidden = isHidden
        tableView.isHidden = !messageLabel.isHidden
        messageLabel.text = messageText
    }
    
}

// MARK: - UITableDataSource
extension SearchViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.response?.results?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = SearchResultTableViewCell(style: .value1, reuseIdentifier: "Cell")
        let result = viewModel.response?.results?[indexPath.row]
        let viewModel = SearchResultTableCellViewModel(result: result)
        cell.updateUI(viewModel: viewModel)
        return cell
    }
}

// MARK: - UITableDelegate
extension SearchViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let result = viewModel.response?.results?[indexPath.row] else { return }
        let mapVC = MapViewController()
        mapVC.result = result
        navigationController?.pushViewController(mapVC, animated: true)
    }
}

// MARK: - UISearchResultsUpdating
extension SearchViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if searchController.searchBar.text?.isEmpty ?? true {
            updateMessageLabel(isHidden: false, messageText: "Search now")
            return
        }
        searchPlaces(with: searchController.searchBar.text ?? "")
    }
}
