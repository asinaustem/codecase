//
//  DetailViewController.swift
//  CodeCaseMarti
//
//  Created by AppLogist on 19.01.2020.
//  Copyright © 2020 AhmetSina. All rights reserved.
//

import UIKit
import Kingfisher

class DetailViewController: UIViewController {

    var result : CCResult?
    lazy var imageView = UIImageView(frame: .zero)
    lazy var iconImageView = UIImageView(frame: .zero)
    lazy var typeLabel = UILabel(frame: .zero)
    lazy var nameLabel = UILabel(frame: .zero)
    lazy var descriptionLabel = UILabel(frame: .zero)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    // MARK: - Setup UI
    /// Call all UI methods
    func setupUI() {
        view.backgroundColor = .white
        setupImageView()
        setupIconImageView()
        setTypeLabel()
        setNameLabel()
        setDescriptionLabel()
    }
    
    func setupNavigationBar() {
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationItem.title = "Detail"
    }
    
    /// Setup Place First Image
    func setupImageView() {
        view.addSubview(imageView)
        guard let imageURL = result?.firstPhotoURL else {
            return
        }
        imageView.kf.setImage(with: imageURL)
        imageView.kf.indicatorType = .activity
        imageView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.leading.equalTo(view.safeAreaLayoutGuide.snp.leading)
            make.trailing.equalTo(view.safeAreaLayoutGuide.snp.trailing)
            make.height.equalTo(imageView.snp.width).multipliedBy(0.8)
        }
    }
    
    func setupIconImageView() {
        view.addSubview(iconImageView)
        guard let imageURL = result?.icon else {
            return
        }
        iconImageView.snp.makeConstraints { make in
            make.top.equalTo(imageView.snp.bottom).offset(16)
            make.leading.equalTo(view.safeAreaLayoutGuide.snp.leading).offset(16)
            make.height.equalTo(30)
            make.width.equalTo(30)
        }
        iconImageView.startAnimating()
        iconImageView.contentMode = .scaleAspectFit
        iconImageView.kf.indicatorType = .activity
        iconImageView.kf.setImage(with: imageURL, options: [.transition(.fade(0.3))])
    }
    /// Setup Place Name Label
    func setTypeLabel() {
        view.addSubview(typeLabel)
        typeLabel.text = result?.type
        typeLabel.font = UIFont.systemFont(ofSize: 10)
        typeLabel.numberOfLines = 0
        typeLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(iconImageView.snp.centerY)
            make.leading.equalTo(iconImageView.snp.trailing).offset(8)
            make.trailing.equalTo(view.safeAreaLayoutGuide.snp.trailing).inset(16)
        }
    }

    /// Setup Place Name Label
    func setNameLabel() {
        view.addSubview(nameLabel)
        nameLabel.text = result?.name
        nameLabel.font = UIFont.boldSystemFont(ofSize: 18)
        nameLabel.numberOfLines = 0
        nameLabel.snp.makeConstraints { (make) in
            if result?.firstPhotoURL != nil {
                make.top.equalTo(iconImageView.snp.bottom).offset(16)
            }else {
                make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(16)
            }
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().inset(16)
        }
    }
    
    /// Setup Place Formatted Address Label
    func setDescriptionLabel() {
        view.addSubview(descriptionLabel)
        descriptionLabel.text = result?.formattedAddress
        descriptionLabel.font = UIFont.systemFont(ofSize: 12)
        descriptionLabel.numberOfLines = 0
        descriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(nameLabel.snp.bottom).offset(16)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().inset(16)
        }
    }
}
