//
//  MapViewController.swift
//  CodeCaseMarti
//
//  Created by AppLogist on 19.01.2020.
//  Copyright © 2020 AhmetSina. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewController: UIViewController {
    
    var result : CCResult?
    var camera : GMSCameraPosition {
        return GMSCameraPosition.camera(withLatitude: result?.geometry?.location?.lat ?? 0,
                                        longitude: result?.geometry?.location?.lng ?? 0,
                                        zoom: 16)
    }
    lazy var mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
    
    override func loadView() {
        super.loadView()
        setupMap()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        createMarker()
    }
    
    // MARK: - Setup UI
    func setupMap() {
        mapView.delegate = self
        view = mapView
    }
    
    /// Creates a marker in the center of the map.
    func createMarker() {
        let marker = GMSMarker()
        guard let location = result?.geometry?.location,
            let lat = location.lat,
            let long = location.lng else {
            return
        }
        marker.position = CLLocationCoordinate2D(latitude: lat,
                                                 longitude: long)
        marker.title = result?.name
        marker.snippet = result?.formattedAddress
        marker.map = mapView
    }
}

// MARK: - GMSMapViewDelegate
extension MapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let detailVC = DetailViewController()
        detailVC.result = result
        navigationController?.pushViewController(detailVC, animated: true)
        return true
    }
}
