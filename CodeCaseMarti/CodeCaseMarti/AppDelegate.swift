//
//  AppDelegate.swift
//  CodeCaseMarti
//
//  Created by AppLogist on 19.01.2020.
//  Copyright © 2020 AhmetSina. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        setGoogleAPIKeys(key: "AIzaSyCMDBSB-omgmnRiIJ28ogQ0TJFDd6UCE7o")
        setInitialVC()
        
        return true
    }
    
    /// Set Initial View Controller
    func setInitialVC() {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        let rootVC = SearchViewController()
        let navVC = UINavigationController(rootViewController: rootVC)
        window?.rootViewController = navVC
    }
    
    
    /// Set Google Services keys
    /// - Parameter key: Google API Key  [more detail](https://developers.google.com/maps/documentation/ios-sdk/get-api-key#add_key)
    func setGoogleAPIKeys(key: String) {
        GMSServices.provideAPIKey(key)
        GMSPlacesClient.provideAPIKey(key)
    }
}

