//
//  CCPlusCode.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 19, 2020

import Foundation

struct CCPlusCode : Codable {
    
    let compoundCode : String?
    let globalCode : String?
    
}
