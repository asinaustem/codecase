//
//  CCPhoto.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 19, 2020

import Foundation

struct CCPhoto : Codable {
    
    let height : Int?
    let htmlAttributions : [String]?
    let photoReference : String?
    let width : Int?
    
}
