//
//  CCLocation.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 19, 2020

import Foundation

struct CCLocation : Codable {
    
    let lat : Double?
    let lng : Double?

}
