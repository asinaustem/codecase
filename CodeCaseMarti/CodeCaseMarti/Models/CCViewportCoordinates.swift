//
//  CCViewportCoordinates.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 19, 2020

import Foundation

struct CCViewportCoordinates : Codable {
    
    let lat : Float?
    let lng : Float?
    
}
