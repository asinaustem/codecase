//
//  CCOpeningHour.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 19, 2020

import Foundation

struct CCOpeningHour : Codable {
    
    let openNow : Bool?
    
}
