//
//  CCBaseResponse.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 19, 2020

import Foundation

struct CCBaseResponse : Codable {
    
    let nextPageToken : String?
    let results : [CCResult]?
    let status : String?
    let errorMessage: String?
    
    var success : Bool {
        return status == "OK" ? true : false
    }
}
