//
//  CCResult.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 19, 2020

import Foundation

struct CCResult : Codable {
    
    let formattedAddress : String?
    let geometry : CCGeometry?
    let icon : URL?
    let id : String?
    let name : String?
    let openingHours : CCOpeningHour?
    let photos : [CCPhoto]?
    let placeId : String?
    let plusCode : CCPlusCode?
    let priceLevel : Int?
    let rating : Float?
    let reference : String?
    let types : [String]?
    let userRatingsTotal : Int?
    
    private var apiKey: String {
        return "AIzaSyCMDBSB-omgmnRiIJ28ogQ0TJFDd6UCE7o"
    }
    
    var firstPhotoURL: URL? {
        guard let firsPhotoReference = photos?.first?.photoReference else {
            return nil
        }
        return URL(string: "https://maps.googleapis.com/maps/api/place/photo?maxwidth=1000&photoreference=\(firsPhotoReference)&key=\(apiKey)")
    }
    
    var type: String? {
        guard let type = types?.first else {
            return nil
        }
        return type.replacingOccurrences(of: "_", with: " ").capitalized
    }
}
