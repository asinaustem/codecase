//
//  CCGeometry.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 19, 2020

import Foundation

struct CCGeometry : Codable {
    
    let location : CCLocation?
    let viewport : CCViewport?
    
}
