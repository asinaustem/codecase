//
//  SearchResultTableCellViewModel.swift
//  CodeCaseMarti
//
//  Created by AppLogist on 20.01.2020.
//  Copyright © 2020 AhmetSina. All rights reserved.
//

import Foundation

class SearchResultTableCellViewModel {
    
    private var result: CCResult?
    
    init(result: CCResult?) {
        self.result = result
    }
    
    var text: String? {
        return result?.name
    }
    
    var detailText: String? {
        return "\(result?.rating ?? 0)"
    }
}
