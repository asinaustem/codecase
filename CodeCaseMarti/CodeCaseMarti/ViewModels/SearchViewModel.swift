//
//  SearchViewModel.swift
//  CodeCaseMarti
//
//  Created by AppLogist on 19.01.2020.
//  Copyright © 2020 AhmetSina. All rights reserved.
//

import Foundation

class SearchViewModel {
    
    private var networkService: NetworkManager?
    
    var response: CCBaseResponse? {
        didSet {
            self.didFinishFetch?()
        }
    }
    
    var error: String? {
        didSet { self.showAlertClosure?(error) }
    }
    
    // MARK: - Closures for callback, since we are not using the ViewModel to the View.
    var showAlertClosure: ((_ message: String?) -> ())?
    var updateLoadingStatus: (() -> ())?
    var didFinishFetch: (() -> ())?
    
    init(service: NetworkManager?) {
        self.networkService = service
    }
    
    func searchPlaces(with query: String?) {
        guard let query = query else { return }
        networkService?.places(query: query, completion: { result in
            switch result {
                case .success(let response):
                    if response.success {
                        self.response = response
                        return
                    }
                    self.error = response.errorMessage
                
                case .failure(let error):
                    debugPrint(error.localizedDescription)
                    self.error = error.localizedDescription
            }
        })
    }
}
