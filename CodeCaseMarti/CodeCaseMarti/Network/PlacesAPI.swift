//
//  PlacesAPI.swift
//  CodeCaseMarti
//
//  Created by AppLogist on 19.01.2020.
//  Copyright © 2020 AhmetSina. All rights reserved.
//

import Moya

enum PlacesAPI {
    case places(query: String)
}

extension PlacesAPI: TargetType {
    var baseURL: URL {
        return URL(string: "https://maps.googleapis.com/maps/api/")!
    }
    
    var path: String {
        return "place/textsearch/json"
    }
    
    var method: Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        if case let PlacesAPI.places(query) = self {
            return .requestParameters(parameters: ["query": query,"key": apiKey],
                                      encoding: URLEncoding.default)
        }
        return .requestPlain
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    var apiKey: String {
        return "AIzaSyCMDBSB-omgmnRiIJ28ogQ0TJFDd6UCE7o"
    }
}
