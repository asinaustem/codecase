//
//  NetworkManager.swift
//  CodeCaseMarti
//
//  Created by AppLogist on 19.01.2020.
//  Copyright © 2020 AhmetSina. All rights reserved.
//

import Moya
import ReactiveSwift
import Result

public typealias APIResult<T> = Result<T, MoyaError>

struct NetworkManager {
    
    static let shared = NetworkManager()
    
    /// Log network data
    static let verbose = true
    
    // MARK: - Provider setup
    
    private let provider = MoyaProvider<PlacesAPI>(plugins: NetworkManager.plugins)
    
    // Pretty print JSON
    static func JSONResponseDataFormatter(_ data: Data) -> Data {
        do {
            let dataAsJSON = try JSONSerialization.jsonObject(with: data)
            let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
            return prettyData
        } catch {
            return data // fallback to original data if it can't be serialized.
        }
    }
    
    static var plugins: [PluginType] = {
        [NetworkLoggerPlugin(verbose: verbose,
                             responseDataFormatter: NetworkManager.JSONResponseDataFormatter)]
    }()
    
    private var jsonDecoder: JSONDecoder {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        return jsonDecoder
    }
    
    private func fetch<M: Decodable>(target: PlacesAPI,
                                            completion: @escaping (_ result: APIResult<M>) -> Void ) {
        provider.reactive.request(target).startWithResult { result in
            self.handleResponse(result, completion: completion)
        }
       }
    
    private func handleResponse<M: Decodable>(_ result: Result<Response, MoyaError>,
                                                  completion: @escaping (_ result: APIResult<M>) -> Void ) {
        switch result {
        case .success(let response):
            do {
                let filteredResponse = try response.filter(statusCodes: 200...401)
                let mappedResponse = try filteredResponse.map(M.self,
                                                               atKeyPath: nil,
                                                               using: jsonDecoder,
                                                               failsOnEmptyData: false)
                DispatchQueue.main.async {
                    completion(.success(mappedResponse))
                }
            } catch MoyaError.statusCode(let response) {
                completion(.failure(MoyaError.statusCode(response)))
            } catch {
                debugPrint("##ERROR parsing##: \(error.localizedDescription)")
                let moyaError = MoyaError.requestMapping(error.localizedDescription)
                completion(.failure(moyaError))
            }
        case .failure(let error):
            debugPrint("##ERROR service:## \(error.localizedDescription)")
            completion(.failure(error))
        }
    }
    
    // MARK: - Places
    func places(query: String, completion: @escaping (_ result: APIResult<CCBaseResponse>) -> Void) {
        fetch(target: .places(query: query) , completion: completion)
    }
    
}

